from random import randint

name = input("Hi! Whats your name? ")
print("Hi", name, "I am  going to try and guess your birth month and birth year!")


for guess_number in range(1, 6):
    guess_month = randint(1, 12)
    guess_year = randint(1924, 2004)
    print("Guess ", guess_number , name, ": were you born in", guess_month, "/", guess_year, "?")
    response = input("yes or no? ")

    if response == "yes":
        print("I knew it!")
        break
    elif guess_number == 5:
        print("I have other things to do, goodbye.")
    elif response == "no":
        print("Drat! Lemme try again.")
